function [ varargout ] = PlotNodes( ax, coord, varargin )
% Function plotting node positions as scatter plot.
%
% If the node coordinates are given only, they will be plotted as black
% dots. If additional color data per node is given, this color data will be
% used to color each node scaled color mapping.
%
% Input Data:
% ax:                 axes object handle
% coord:              nx2 matrix with n node coordinates (x,y)
% varargin{1}: cData: colorData for coloring each point using scaled color
%                     mapping
%
% Output Data:
% varargout{1}: axes object handle
% varargout{2}: scattergroup object handle
% varargout{3}: colorbar object handel
%
% [varargout] = PlotNodes(ax, coord, varargin)

% created by Henning Schippke, 03.02.15
% last modification            20.03.15


if isempty(ax)
    ax = axes();
else
    set(gcf,'CurrentAxes',ax);
end

markerSize  = 20;
markerColor = 'k';
markerStyle = 'filled';

if isempty(varargin) % no color data specified
    
     scat = scatter(coord(:,1),coord(:,2),markerSize,markerColor,markerStyle);
    
else % node-based color data
    
    cData = varargin{1};
    scat  = scatter(coord(:,1),coord(:,2),markerSize,cData,markerStyle);
    
    cbar         = colorbar;
    varargout{3} = cbar;

end

% hier Abfrage, ob plot nur 2D
axis equal tight


varargout{1} = ax;
varargout{2} = scat;

end %function PlotNodes