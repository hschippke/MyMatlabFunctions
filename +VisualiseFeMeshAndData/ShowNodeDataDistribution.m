function [ varargout ] = ShowNodeDataDistribution( nodes          , ...
                                                   inziConnect    , ...
                                                   nodeDataList   , ...
                                                   nodeDataStruct )
% Function showing a 2D (finite element) mesh with colored nodes. Only those
% nodes are plotted which occur in nodeDataList. The coloring is related to
% the (initial or boundary) value specified at this node. For each dofID a
% separate figure is created.
%
% Input Data:
% nodes                : n x 2 matrix with n coordinates (x,y)
% inziConnect          : m x 1 cell with connectivity data for m elements
% nodeDataList         : k x 3 matrix with nodal data
%                        nodeDataList(1,:) : node number
%                        nodeDataList(2,:) : dofID
%                        nodeDataList(3,:) : value
% nodeDataStruct       : structure containing control data for labelling and
%                        plotting (optional)
%     .label.title     : figure name and title of plot
%     .label.x         : x-axis label
%     .label.y         : y-axis label
%     .label.cbar      : colorbar label
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .saveName        : file name for saving
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout     : k x 4 cell with handle object for k figures (each related
%                 to k dofID nodal data distribution
% varargout{k,1}: figure   handle object
% varargout{k,2}: axes     handle object
% varargout{k,3}: patch    handle object (lines of fe mesh)
% varargout{k,4}: colorbar handle object
%
% [ varargout ] = ShowNodeDataDistribution( nodes, inziConnect, nodeDataList, nodeDataStruct )

% created by Henning Schippke, 21.03.15
% last modification            22.08.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*

   
% -- Read out optional input values ------------------------------------- %

% show or hide figure
if isfield(nodeDataStruct,'flagFigVisible')
    flagFigVisible = nodeDataStruct.flagFigVisible;
else
    flagFigVisible = 'on';
end

% figure position on screen
if isfield(nodeDataStruct,'figPos')
    figPos = nodeDataStruct.figPos;
else
    figPos = [];
end

% flag for saving figures as *.pdf and *.fig
if isfield(nodeDataStruct,'flagSaveFigures')
    flagSaveFigures = nodeDataStruct.flagSaveFigures;
else
    flagSaveFigures = 0;
end

% create also *.fig file
if isfield(nodeDataStruct,'flagCreateFigFiles')
    flagCreateFigFiles = nodeDataStruct.flagCreateFigFiles;
else
    flagCreateFigFiles = 0;
end

% ----------------------------------------------------------------------- %


% find out which dofIDs are prescribed
dofIDList = unique(nodeDataList(:,2));
numbDofId = length(dofIDList);


varargout = cell(numbDofId,4); % preallocation for return values

% plot nodal distribution of each prescribed dofID
for ii = 1:numbDofId
   
    dofID = dofIDList(ii); % current dofID
    
    % get nodeIDs which have currDofID prescribed
    nodeIDsDataList = find(nodeDataList(:,2) == dofID);    
    nodeIDs         = nodeDataList(nodeIDsDataList,1);
    
    
    % create figure
    fig = figure('visible',flagFigVisible,'Name',[nodeDataStruct.label.title,' - dofID ',num2str(dofID)],'NumberTitle','off');
    if ~isempty(figPos)
        position    = nodeDataStruct.figPos;
        position(2) = position(2) - 25 * (ii-1);
        set(fig,'Position',position);
    end
    
    
    % show fe mesh with colored nodes related to node data distribution
    ax = axes(); hold(ax,'on');    %#ok<LAXES>
    [~, ptch   ] = PlotFiniteElements(ax, nodes, inziConnect);  
    [~, ~, cbar] = PlotNodes(ax, nodes(nodeIDs,1:2), nodeDataList(nodeIDsDataList,3));    
    hold(ax,'off');
    
    
    % label
    title( ax, [nodeDataStruct.label.title,' - dofID ',num2str(dofID)]); 
    xlabel(ax,  nodeDataStruct.label.x); 
    ylabel(ax,  nodeDataStruct.label.y);
    
    set(get(cbar,'ylabel'),'string','value [dofID dependent]')
    
    
    % save figures to file (if wanted)
    if flagSaveFigures == 1
        savePath    = nodeDataStruct.savePath;
        saveNameFig = [nodeDataStruct.saveName,'_','dofID-',num2str(dofID)];

        saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])    

        if flagCreateFigFiles == 1
            saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
        end
        
    end %if
    
    
    % return values
    varargout{ii,1} = fig;
    varargout{ii,2} = ax;
    varargout{ii,3} = ptch;
    varargout{ii,4} = cbar;
    
end %for 
      

end %function ShowInitialCondition

% ======================================================================= %
% ======================================================================= %