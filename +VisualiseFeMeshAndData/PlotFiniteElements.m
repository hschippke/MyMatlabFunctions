function [ varargout ] = PlotFiniteElements( ax, coord, connect, varargin )
% Function plotting the discretised spatial geometry via finite elements.
%
% If node coordinates and connectivity data is given only, the elements are
% plotted using the patch function with black edge color and white faces.
% In case additional color data per node or per element is specified this
% data will be used to color the element flat or interpolated using a
% scaled color mapping.
%
% Input Data:
% ax:          axes object handle
% coord:       n x 2 matrix with n coordinates (x,y)
% connect:     m x var cell or matrix with connectivity data for m elements
%              with variable number of nodes
% varargin{1}: var x 1 color data vector used for scaled color mapping
%              var = n: color data is nodal   based (shadding = interp)
%              var = m: color data is element based (shadding = flat)
% varargin{2}: vertex z-coord data
%              If given, the 2D elements are plotted as 3D patches with
%              color data according to varargin{1} and heights using
%              varargin{2}
%
% Output Data:
% varargout{1}: axes object handle
% varargout{2}: patch object handle
% varargout{3}: colorbar object handel
%
% [varargout] = PlotFiniteElements(ax, coord, connect, varargin)

% created by Henning Schippke, 03.02.15
% last modification            20.03.15


% Convert cell to matrix (if necessary)
if iscell(connect)
    connect = FEM2FlowUtilities.Connectivity.ConvertConnectivity_cell2mat(connect);
end

% Plotting of finite elements
if isempty(ax)
    ax = axes();
else
    set(gcf,'CurrentAxes',ax);
end

if isempty(varargin) % no color data specified
    
    ptch = patch('Vertices',coord,'Faces',connect,'EdgeColor','k','FaceColor','w');

else % color data is specified on node or element bases
    
    vertexColorData = varargin{1};
    
    if length(varargin) > 1
        varargin{2} = vertexCoordZData;
        coord      = [coord, vertexCoordZData];
    end
    
    ptch = patch('Vertices',coord,'Faces',connect);
            
    set(ptch,'FaceVertexCData',vertexColorData);
    
    if length(vertexColorData) == size(coord,1)      % colorData is nodal-based
        set(ptch,'FaceColor','interp');
    else %length(vertexColorData) == size(connect,1) % colorData is element-based
        set(ptch,'FaceColor','flat')
    end
    
    cbar_axes    = colorbar;
    varargout{3} = cbar_axes;
          
end


if length(varargin) <= 1 % 2D plot
    axis equal
    axis([min(coord(:,1)), max(coord(:,1)), min(coord(:,2)), max(coord(:,2))]);
end


varargout{1} = ax;
varargout{2} = ptch;

end %function PlotFiniteElements