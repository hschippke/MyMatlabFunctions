function  [ varargout ] = ShowNodeConnectivity(coord, connect, controlStruct)
% Function showing the node connectivity of a (finite element) mesh via its
% connectivity matrix.
%
% Input Data:
% coord:         n x 2 matrix with n coordinates (x,y)
% connect:       m x 1 cell with connectivity data for m elements
%                with variable number of nodes per element
% controlStruct: controlStructure used for plotting (optional)
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout{1}: figure  handle object
% varargout{2}: axes    handle object
% varargout{3}: spy     handle object (blue dots in spy-plot)
%
% [ varargout ] = ShowNodeConnectivity(coord, connect, controlStruct)

% created by Henning Schippke, 20.03.15
% last modification            22.08.15


% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*

% check, if control structure exists and if yes
% read out input values or set default values
if ~isempty(controlStruct)
    
    % show or hide figure
    if isfield(controlStruct,'flagFigVisible')
        flagFigVisible = controlStruct.flagFigVisible;
    else
        flagFigVisible = 'on';
    end
    
    % figure position on screen
    if isfield(controlStruct,'figPos')
        figPos = controlStruct.figPos;
    else
        figPos = [];
    end
    
    % flag for saving figures as *.pdf and *.fig
    if isfield(controlStruct,'flagSaveFigures')
        flagSaveFigures = controlStruct.flagSaveFigures;
    else
        flagSaveFigures = 0;
    end
 
    % set figure save name
    if isfield(controlStruct,'saveName')
        saveNameFig = controlStruct.saveName;
    else
        saveNameFig = 'nodeConnectivity';
    end
    
    % set figure title
    if isfield(controlStruct,'figTitle')
        figTitle = controlStruct.figTitle;
    else
        figTitle = 'connectivity matrix';
    end
        
    % create also *.fig file
    if isfield(controlStruct,'flagCreateFigFiles')
        flagCreateFigFiles = controlStruct.flagCreateFigFiles;
    else
        flagCreateFigFiles = 0;
    end
      
end

numbNodes = size(coord,1);

% convert connectivity from cell to matrix (if necessary)
if iscell(connect)
    connect = FEM2FlowUtilities.Connectivity.ConvertConnectivity_cell2mat(connect);
end

% compute connectivity matrix
[nodeConnectStruct, bandwidth ] = FeMeshAnalysis.ComputeNodeConnectivityMatrix(connect);

K         = nodeConnectStruct.K;
K_nz      = nodeConnectStruct.K_nz;
K_dim     = nodeConnectStruct.K_dim;
K_density = nodeConnectStruct.K_density;

% plot bandwith
fig = figure('visible',flagFigVisible,'Name',figTitle,'NumberTitle','off');
if ~isempty(figPos)
    set(fig,'position', figPos)
end

ax = axes();

% show node connectivity matrix
spy(K);
spyHandle = findall(ax,'color','b');

% label
title( ax,figTitle);
xlabel(ax                                                         , ...
       {['bandwidth: ',                                             ...
         num2str(bandwidth.value), ' / ', num2str(numbNodes),' = ', ...
         num2str(bandwidth.relative),' %'                           ...
        ]                                                           ...
        ['degree of density: ',                                     ...
         num2str(K_nz),' / ',num2str(K_dim),'\^2', '  = ',          ...
         num2str(K_density), ' %'                                   ...
        ]                                                           ...
      });

  
% save figures to file (if wanted)
if flagSaveFigures == 1
    
    savePath    = controlStruct.savePath;
    
    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])
    
    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end
    
end


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = spyHandle;

end %function ShowNodeConnectivity

% ======================================================================= %
% ======================================================================= %