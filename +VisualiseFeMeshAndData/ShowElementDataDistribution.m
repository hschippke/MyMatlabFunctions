function [ varargout ] = ShowElementDataDistribution( nodes            , ...
                                                      inziConnect      , ...
                                                      elementDataList  , ...
                                                      elementDataStruct )
% Function showing a 2D (finite element) mesh with colored elements related
% to the distribution of the element data given in elementDataList.
%
% Input Data:
% nodes                : n x 2 matrix with n coordinates (x,y)
% inziConnect          : m x 1 cell with connectivity data for m elements
% elementDataList      : m x 1 vector with element data for m elements
% elementDataStruct    : structure containing control data for labelling and
%                        plotting (optional)
%     .label.title     : figure name and title of plot
%     .label.x         : x-axis label
%     .label.y         : y-axis label
%     .label.cbar      : colorbar label
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .saveName        : file name for saving
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout{1}: figure   handle object
% varargout{2}: axes     handle object
% varargout{3}: patch    handle object (lines of fe mesh)
% varargout{4}: colorbar handle object
%
% [ varargout ] = ShowElementDataDistribution( nodes, inziConnect, elementDataList, elementDataStruct )

% created by Henning Schippke, 21.03.15
% last modification            22.08.15


% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*


% -- Read out optional input values ------------------------------------- %

% show or hide figure
if isfield(elementDataStruct,'flagFigVisible')
    flagFigVisible = elementDataStruct.flagFigVisible;
else
    flagFigVisible = 'on';
end

% figure position on screen
if isfield(elementDataStruct,'figPos')
    figPos = elementDataStruct.figPos;
else
    figPos = [];
end

% flag for saving figures as *.pdf and *.fig
if isfield(elementDataStruct,'flagSaveFigures')
    flagSaveFigures = elementDataStruct.flagSaveFigures;
else
    flagSaveFigures = 0;
end

% create also *.fig file
if isfield(elementDataStruct,'flagCreateFigFiles')
    flagCreateFigFiles = elementDataStruct.flagCreateFigFiles;
else
    flagCreateFigFiles = 0;
end  
    
% ----------------------------------------------------------------------- %


fig = figure('visible',flagFigVisible, 'Name',elementDataStruct.label.title,'NumberTitle','off');
if ~isempty(figPos)
    set(fig,'Position',figPos);
end

ax = axes();


% show finite element mesh with elements colored by elementData
[~, ptch, cbar] = PlotFiniteElements(ax, nodes, inziConnect, elementDataList);

% label
title(ax, elementDataStruct.label.title); 
 
xlabel(ax, elementDataStruct.label.x); 
ylabel(ax, elementDataStruct.label.y); 
   
set(get(cbar,'ylabel'),'string',elementDataStruct.label.cbar)


% save figures to file (if wanted)
if flagSaveFigures == 1
    savePath    = elementDataStruct.savePath;
    saveNameFig = ['elementDataDistribution_', elementDataStruct.saveName];

    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])    
  
    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end

end %if


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = ptch;
varargout{4} = cbar;

end %function ShowElementDataDistribution

% ======================================================================= %
% ======================================================================= %