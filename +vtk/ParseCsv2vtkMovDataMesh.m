function [] = ParseCsv2vtkMovDataMesh(csvDataFolder, vtkFileData, parallelExecution)
% Description:
% ---------------
% Function creating a vtk-file from csv data files for representing finite
% elements in Paraview. The node numbers are displayed as vtk point data field,
% whereas element number, material id and load id are displayed as vtk cell
% data fields.
%
% Input data:
% --------------
% csvDataFolder :  string containing path to folder with csv-files
%
% vtkFileData : structure with output path data and flags
%     .outputDataFolder : string with path to ouput folder
%     .outputFileName   : string with file name to created (without extension)
%     .flag
%          .writePointData : integer specifying if point data is stored in
%                            vtk-file
%                            0: no, 1:yes
%          .writeCellData  : integer specifying if cell data is stored in
%                            vtk-file
%                            0: no, 1:yes
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = ParseCsv2vParseCsv2vtkMovDataMeshtk_meshData(csvDataFolder, vtkFileData, parallelExecution)

% created by   : Henning Schippke
% created on   : 15.04.17
% last modified: 15.04.17

% TO DO:
% - ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Read csv data files ------------------------------------------------ %

import FEM2FlowUtilities.CsvInputDataFiles.LoadCsvData

coord        = LoadCsvData(csvDataFolder, 'nodeCoordinates.csv' );
connectMat   = LoadCsvData(csvDataFolder, 'meshMovSSL.csv');
elementTable = LoadCsvData(csvDataFolder, 'elementTable.csv'    );

% ----------------------------------------------------------------------- %


% -- Rearrange connectivity --------------------------------------------- %

% change connectivity data from matrix to cell type
connect = FEM2FlowUtilities.Connectivity.CreateConnectivityCellExt(connectMat);

% extend connectivity with elementTypeID
connectExt = cell(size(connect,1),3);

for mm = 1:size(connect,1)

    elementNumber    = connect{mm,1};
    elementTypeId    = elementTable( elementTable(:,1) == elementNumber, 2);

    connectExt{mm,1} = connect{mm,1};
    connectExt{mm,2} = connect{mm,2};
    connectExt{mm,3} = elementTypeId;

end

% ----------------------------------------------------------------------- %


% -- Node data fields --------------------------------------------------- %

nodeData = [];
% nodeData(1).fieldName  = 'elementFirstNode';
% nodeData(1).dataType   = 'Int64';
% nodeData(1).dataFormat = '%6i';
% nodeData(1).nComp      = '1';
% nodeData(1).data       = [nodeA, value];
% ----------------------------------------------------------------------- %


% -- Element data fields ------------------------------------------------ %

elementNumbersSSL    = connect(:,1);
elementNumbersSSL    = cell2mat(elementNumbersSSL(:,1));

nElementNumbersSSL   = length(elementNumbersSSL);


% local element ordering
localElementOrdering = linspace(1, nElementNumbersSSL, nElementNumbersSSL)';

elementData(1).fieldName  = 'elementOrdering';
elementData(1).dataType   = 'Int64';
elementData(1).dataFormat = '%6i';
elementData(1).nComp      = '1';
elementData(1).data       = [elementNumbersSSL, localElementOrdering];


% element type ID
[row, ~] = find(elementTable(:,1) == elementNumbersSSL');

elementData(2).fieldName  = 'elementTypeID';
elementData(2).dataType   = 'Int64';
elementData(2).dataFormat = '%4i';
elementData(2).nComp      = '1';
elementData(2).data       = elementTable(row,1:2);

% ----------------------------------------------------------------------- %


% -- Write csv data to vtk file ----------------------------------------- %

vtk.WriteVtkFile.WriteVtkFile(vtkFileData, coord, connectExt, nodeData, elementData, parallelExecution);

% ----------------------------------------------------------------------- %

end


% ======================================================================= %
% ======================================================================= %
