function [] = ParseCsv2vtkMovDataNode(csvDataFolder, vtkFileData, fileName, parallelExecution)
% Description:
% ---------------
% Function creating a vtk-file from csv data for representing the node data
% in Paraview.
%
% Input data:
% --------------
% csvDataFolder :  string containing path to folder with csv-files
%
% vtkFileData : structure with output path data and flags
%     .outputDataFolder : string with path to ouput folder
%     .outputFileName   : string with file name to created (without extension)
%     .flag
%          .writePointData : integer specifying if point data is stored in
%                            vtk-file
%                            0: no, 1:yes
%          .writeCellData  : integer specifying if cell data is stored in
%                            vtk-file
%                            0: no, 1:yes
%
% fileName : string containing filename of file to be loaded
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = ParseCsv2vtk_nodeData(csvDataFolder, vtkFileData, fileName, parallelExecution)

% created by   : Henning Schippke
% created on   : 13.04.17
% last modified: 20.04.17

% TO DO:
% - ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Read csv data files ------------------------------------------------ %

import FEM2FlowUtilities.CsvInputDataFiles.LoadCsvData

nodes        = LoadCsvData(csvDataFolder, 'nodeCoordinates.csv' );
movDataArray = LoadCsvData(csvDataFolder, fileName );

% ----------------------------------------------------------------------- %

if ~isempty(movDataArray)

% -- Reduce coordinate matrix --------------------------------------- %

% get nodeIDs
if strcmp(fileName, 'meshMovSSL.csv')
    connectMat = movDataArray;
    nodeA      = connectMat(:,2);
    nodeIDs    = nodeA;
else
    nodeIDs    = movDataArray;
end

nNode   = length(nodeIDs);

% get node coordinates of Dirichlet bc nodes
nodes_red = zeros(nNode, size(nodes,2));
for nn = 1:nNode
   nodeNumb        = nodeIDs(nn);
   nodes_red(nn,:) = nodes( nodes(:,1) == nodeNumb, :);

end
% ------------------------------------------------------------------- %


% -- Create dummy connectivity -------------------------------------- %

inziConnect   = cell(size(nodes_red,1),3);

elementTypeId = 1; % dummy element type id (1: point) for vtk point data

for mm = 1:size(inziConnect,1)

   inziConnect{mm,1} = nodes_red(mm,1); % element number == node number
   inziConnect{mm,2} = nodes_red(mm,1); % element connectivity == only node itself
   inziConnect{mm,3} = elementTypeId;

end
% ------------------------------------------------------------------- %


switch fileName

    case 'meshMovNodes.csv'

    % -- Node data fields ----------------------------------------------- %

    nodeData = [];
    % nodeData(1).fieldName  = vtkFileData.outputFileName;
    % nodeData(1).dataType   = 'Int32';
    % nodeData(1).dataFormat = '%6d';
    % nodeData(1).nComp      = '1';
    % nodeData(1).data       = [nodeIDs, values];

    % ------------------------------------------------------------------- %


    % -- Element data fields -------------------------------------------- %

    elementData = [];
    % elementData(1).fieldName  = 'elementTypeID';
    % elementData(1).dataType   = 'Float32';
    % elementData(1).dataFormat = '%8.4f';
    % elementData(1).nComp      = '1';
    % elementData(1).data       = elementTable(:,1:2);

    % ------------------------------------------------------------------- %


    case 'meshMovNodesBound.csv'

    % -- Node data fields ----------------------------------------------- %

    nodeData = [];
    % nodeData(1).fieldName  = vtkFileData.outputFileName;
    % nodeData(1).dataType   = 'Int32';
    % nodeData(1).dataFormat = '%6d';
    % nodeData(1).nComp      = '1';
    % nodeData(1).data       = [nodeIDs, values];

    % ------------------------------------------------------------------- %


    % -- Element data fields -------------------------------------------- %

    elementData = [];
    % elementData(1).fieldName  = 'elementTypeID';
    % elementData(1).dataType   = 'Float32';
    % elementData(1).dataFormat = '%8.4f';
    % elementData(1).nComp      = '1';
    % elementData(1).data       = elementTable(:,1:2);

    % ------------------------------------------------------------------- %


    case 'meshMovSSLNodesInt.csv'

    % create local node ordering
    values  = linspace(1, nNode, nNode)';


    % -- Node data fields ----------------------------------------------- %

    % Initial values
    nodeData(1).fieldName  = vtkFileData.outputFileName;
    nodeData(1).dataType   = 'Int32';
    nodeData(1).dataFormat = '%4d';
    nodeData(1).nComp      = '1';
    nodeData(1).data       = [nodeIDs, values];

    % ------------------------------------------------------------------- %


    % -- Element data fields -------------------------------------------- %

    elementData = [];
    % elementData(1).fieldName  = 'elementTypeID';
    % elementData(1).dataType   = 'Float32';
    % elementData(1).dataFormat = '%8.4f';
    % elementData(1).nComp      = '1';
    % elementData(1).data       = elementTable(:,1:2);

    % ------------------------------------------------------------------- %

    case 'meshMovSSL.csv'

    % -- Node data fields ----------------------------------------------- %

    nodeData = [];
    % nodeData(1).fieldName  = vtkFileData.outputFileName;
    % nodeData(1).dataType   = 'Int32';
    % nodeData(1).dataFormat = '%6d';
    % nodeData(1).nComp      = '1';
    % nodeData(1).data       = [nodeIDs, values];

    % ------------------------------------------------------------------- %


    % -- Element data fields -------------------------------------------- %

    elementData = [];
    % elementData(1).fieldName  = 'elementTypeID';
    % elementData(1).dataType   = 'Float32';
    % elementData(1).dataFormat = '%8.4f';
    % elementData(1).nComp      = '1';
    % elementData(1).data       = elementTable(:,1:2);

    % ------------------------------------------------------------------- %

    otherwise

        error('Only "meshMovNodes.csv", "meshMovSSLNodesInt.csv" and "meshMovSSL.csv" are defined as case for function "ParseCsv2vtkMovDataNode()."')

end %switch

% -- Write csv data to vtk file ------------------------------------- %

vtk.WriteVtkFile.WriteVtkFile(vtkFileData, nodes_red, inziConnect, nodeData, elementData, parallelExecution);

% ------------------------------------------------------------------- %

end %if

end %function


% ======================================================================= %
% ======================================================================= %
