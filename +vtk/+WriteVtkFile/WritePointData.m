function [] = WritePointData(fid, indent, vtkPointCoord, nodeVtkIdTable, varargin )
% Description:
% ---------------
% Function writing point data to vtk-file.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkPointCoord : nVtkPoint x 4 matrix
%                [ vtkPointId | xCoord | yCoord | zCoord ]
%
% nodeVtkIdTable : nNode x 2 matrix
%                  [ nodeNumber | vtkPointId ]
%
% varargin{1} = nodeData : nNodeData x 1 structured array
%     nodeData(ii).fieldName  : name of point data field
%     nodeData(ii).dataType   : data type : (U)Int8/16/32/64, Float32/64 
%     nodeData(ii).dataFormat : data format string, e.g. %8.4f
%     nodeData(ii).nComp      : number of components per point
%     nodeData(ii).data       : vtk point data array
%                               nVtkPoint x 2 matrix
%                               [ nodeNumber | dataValue ]
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WritePointData(fid, indent, vtkPointCoord, nodeVtkIdTable, varargin )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... writing point data         to vtk file ...');


fprintf(fid, [indent('3'), '<PointData Scalars="nodeNumber">\n']);

% -- Write node numbers as node data field ------------------------------ %

nodeData.fieldName  = 'nodeNumber';
nodeData.dataType   = 'Int64';
nodeData.dataFormat = '%8i';
nodeData.nComp      = '1';
nodeData.data       = [nodeVtkIdTable(:,1), nodeVtkIdTable(:,1)];

WritePointData_Scalars(fid, indent, vtkPointCoord, nodeVtkIdTable, nodeData);

% ----------------------------------------------------------------------- %


% -- Write node coordinates as node data field -------------------------- %

nodeData.fieldName  = 'nodeCoord';
nodeData.dataType   = 'Float64';
nodeData.dataFormat = '%3.5f';
nodeData.nComp      = '3';
nodeData.data       = [];

WritePointData_Coord(fid, indent, vtkPointCoord, nodeData);

% ----------------------------------------------------------------------- %


% -- Write further node data fields ------------------------------------- %

if ~isempty(varargin)
    
    nodeData = varargin{1};
    
    for ii = 1:length(nodeData)
        WritePointData_Scalars(fid, indent, vtkPointCoord, nodeVtkIdTable, nodeData(ii));
    end
    
end

% ----------------------------------------------------------------------- %


fprintf(fid, [indent('3'), '</PointData>\n']);

end %function

% ======================================================================= %


% ======================================================================= %
% Subfunction
% ======================================================================= %

function [] = WritePointData_Scalars(fid, indent, nodes_vtk, nodeVtkIdTable, nodeData)
% Function writing scalar point data to vtk-file (only 1 value per point,
% i.e. nodeData(ii).nComp = 1.

% get field data
fieldName  = nodeData.fieldName;
dataType   = nodeData.dataType;
dataFormat = nodeData.dataFormat;
nComp      = nodeData.nComp;
data       = nodeData.data;
format     = 'ascii';

fprintf(fid, [indent('4'), '<DataArray type="'              ,dataType  ,'" ', ...
                                      'Name="'              ,fieldName ,'" ', ...
                                      'NumberOfComponents="',nComp     ,'" ', ...
                                      'format="'            ,format    ,'"' , ...
                            '>\n']);
     
% loop over all points and write corresponding data
nVtkPoints = size(nodes_vtk,1);

dataFormatString = '';
for ii = 1:str2num(nComp)
  dataFormatString = [dataFormatString, ' ', dataFormat];
end

for ii = 1:nVtkPoints
    
    nodeVtkId     = nodes_vtk(ii,1);
    nodeNumber    = nodeVtkIdTable(nodeVtkIdTable(:,2) == nodeVtkId, 1);  
    
    nodeDataValue = data( data(:,1) == nodeNumber, 2:(2+str2num(nComp)-1) ); 
    
    fprintf(fid, [indent('5'), dataFormatString,'\n'], nodeDataValue);
    
end


fprintf(fid, [indent('4'), '</DataArray>\n']);


end %function

% ======================================================================= %


function [] = WritePointData_Coord(fid, indent, nodes_vtk, nodeData)
% Function writing node coordinates as point data.

% get field data
fieldName  = nodeData.fieldName;
dataType   = nodeData.dataType;
dataFormat = nodeData.dataFormat;
nComp      = nodeData.nComp;
format     = 'ascii';

fprintf(fid, [indent('4'), '<DataArray type="'              ,dataType  ,'" ', ...
                                      'Name="'              ,fieldName ,'" ', ...
                                      'NumberOfComponents="',nComp     ,'" ', ...
                                      'format="'            ,format    ,'"' , ...
                            '>\n']);
     
% loop over all points and write corresponding data
nVtkPoints = size(nodes_vtk,1);

dataFormatString = '';
for ii = 1:str2num(nComp)
  dataFormatString = [dataFormatString, ' ', dataFormat];
end

for ii = 1:nVtkPoints
       
    nodeVtkId     = nodes_vtk(ii,1);
    nodeDataValue = nodes_vtk( nodes_vtk(:,1) == nodeVtkId, 2:(2+str2num(nComp)-1) );
    
    fprintf(fid, [indent('5'), dataFormatString,'\n'], nodeDataValue);
    
end


fprintf(fid, [indent('4'), '</DataArray>\n']);


end %function

% ======================================================================= %


% ======================================================================= %
% ======================================================================= %