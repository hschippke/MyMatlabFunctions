function [] = CloseVtkFile(fid, indent, vtkFileData)
% Description:
% ---------------
% Function closing a vtk-file and writing the footer lines of
% that file in xml-style.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkFileData : control data structure
%     .outputDataFolder : string containing the path to the folder where
%                         the vtu-file will be stored
%     .outputFileName   : file name of vtu-file
%     .flag
%         .writePointData : flag for writing node data as point data to
%                           vtu-file
%                           0 : no, 1: yes
%         .writeCellData  : flag for writing element data as cell data to
%                           vtu-file
%                           0 : no, 1: yes
%     .dataType      : vtk data type, e.g. 'Unstructured Grid'
%     .fileExtension : vtk file extension, e.g. 'vtu'
%     .version       : vtk version, e.g. '0.1'
%     .byteOrder     : machine byte ordering ('LittleEndian', BigEndian')
%     .nVtkPoints    : number of vtk points
%     .nVtkCells     : number of vtk cells
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = CloseVtkFile(fid, indent, vtkFileData)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... close vtk file ...');

vtkDataType         = vtkFileData.dataType;

fprintf(fid, [indent('2'), '</Piece>\n']);
fprintf(fid, [indent('1'), '</',vtkDataType,'>\n']);

fprintf(fid, '</VTKFile>');

fclose(fid);


end

% ======================================================================= %
% ======================================================================= %