function [ d ] = CalcLevelSetFieldPoint2Point(nodes, xInterface, hInterface)
% Function calculating the shortest distance of every point in 'nodes' to
% the interface defined via the (xInterface, hInterface) value pairs.
%
%
% Input data:
% --------------
%
% nodes: (nNodes x 3) matrix
%        [nodeNumber | xCoord | yCoord]
%
% xInterface: (nInterfacePoint x 1) matrix
%             x-coordinates of interface points
%
% hInterface: (nInterfacePoint x 1) matrix
%              y-coordinates of interface points
%
%
% Output data:
% ---------------
%
% d: (nNodes x 1) matrix
%    shortest distance of every node in 'nodes' to the interface
%
%
% Usage:
% ---------
% [ d ] = CalcLevelSetFieldPoint2Point(nodes, xInterface, hInterface)

% created  by Henning Schippke
% created  on spring 2014
% modified on 22.08.2015

% ======================================================================= %
% ======================================================================= %


numNodes = size(nodes,1);

d = zeros(numNodes,2); % Vektor saemtlicher Abstaende zum Nullniveau
                       % je Knotenpunkt

for nn = 1:numNodes    % Schleife �ber alle Knotenpunkte

    nodeNumb = nodes(nn,1); % Knotennummer
    a        = nodes(nn,2); % x-Koordinate aktueller Knotenpunkt
    b        = nodes(nn,3); % y-Koordinate aktueller Knotenpunkt
    
    
    % Abstaende akt. Knoten - Nullniveau
    dist = sqrt( (xInterface - a).^2 + (hInterface - b).^2 );
    
    % Nr. x-Position mit kleinstem Abstand
    c = find( dist == min(dist) );
     
    % Vorzeichen anhand der y-Position (Vgl. LS zu Knoten)
    vz = sign( hInterface(c(1)) - b );
     
    % Vorzeichenbehafteter min. Abstand akt. Knoten - Nullniveau
    d(nn,2) = -vz * dist(c(1));
    d(nn,1) = nodeNumb;
    
end %for

end %function