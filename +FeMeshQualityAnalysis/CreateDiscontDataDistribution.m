function [coordDisct, connectDisct, lutNodeNumber] = CreateDiscontDataDistribution(coord, connect)
% Function generating a discontinuous connectivity for a given continuous node connectivity.
%
% Input data:
% --------------
%
% coord:         nNodes x 4 matrix
%                [ nodeNumber | xCoord | yCoord | zCoord ] % zCoord is optional
%
% connect:       nEle x (nNodePerEle+1) matrix
%                [ eleNumber | nodeA | ... | nodeZ ]
%
%
% Output data:
% ---------------
%
% coordDisct:    nNodesDisct x 4 matrix
%                [ nodeNumbDiscont | xCoord | yCoord | zCoord ] % zCoord is optional
%
% connectDisct:  nEle x (nNodePerEle+1) matrix
%                [ eleNumber | nodeA | ... | nodeZ ]
%
% lutNodeNumber: nNodesDisct x 3 matrix
%                [ eleNumb | nodeNumbCont | nodeNumbDiscont ]
%
%
% Usage:
% ---------
%
% [coordDisct, connectDisct, lutNodeNumber] = CreateDiscontDataDistribution(coord, connect)

% created  by Henning Schippke
% created  on 10.07.2015
% modified on 06.02.2016

% TO DO:
% - extend input data by nodal data (displacements, velocity, ...) and map continuous nodal data
%   to discontinuous connectivity likewise 

% ================================================================= %
% ================================================================= %


nEle           = size(connect,1);
nNodesPerEle   = size(connect,2)-1;
coordDisctNumb = nEle * nNodesPerEle; % nNodes for discont connectivity 

nNode     = size(coord,1);
dimension = size(coord,2)-1;

coordDisct     = zeros(coordDisctNumb,dimension+1);
connectDisct   = zeros(size(connect));

% nDataValuePerNodeOrEle = size(data,2);
% 
% dataDisct      = zeros(coordDisctNumb, nDataValuePerNodeOrEle);

lutNodeNumber = zeros(coordDisctNumb,3);

cc = 1; %newNodeNumber
for jj = 1:nEle %loop over all elements

    currEleNumb = connect(jj,1);
    currNodes   = connect(jj,2:end);
    
%     if size(data,1) == nEle % data is element based
%         
%         currData      = data(jj);
%         currDataNodes = currData * ones(1,length(currNodes));
%         
%     elseif size(data,1) == nNode  % data is nodal based
%         
%         currDataNodes = data(jj,:);
%   
%     end
    
    % elementwise/discontinuous node numbering
    ccBegin = cc;
    ccEnd   = cc + length(currNodes)-1; 
    
    newNodeNumbers = cc:ccEnd;

    
    lutNodeNumber(ccBegin:ccEnd,1) = currEleNumb;
    lutNodeNumber(ccBegin:ccEnd,2) = currNodes;
    lutNodeNumber(ccBegin:ccEnd,3) = ccBegin:ccEnd;
    
    connectDisct(jj,:)           = [currEleNumb, ccBegin:ccEnd];     
    
    for ii = 1:length(newNodeNumbers)
        
        coordDisct(newNodeNumbers(ii),:)  = [newNodeNumbers(ii), coord(coord(:,1) == currNodes(ii),2:end)];
    end
    
%     dataDisct(ccBegin:ccEnd)     = currDataNodes;

    cc = cc + length(currNodes);
end

end