function [connectCellExt] = CreateConnectivityCellExt(connectMatExt)
% Description:
% ---------------
% Function creating an (extented) connectivity cell array from an
% (extented) connectivity matrix.
%
% Input data:
% --------------
% connectMat : nEle x (elementNumber + nNodesPerElement) matrix
%
% Output data:
% ---------------
% connectCellExt : nEle x 2 cell array
%                  { elementNumber | connectivity }
%
% Usage:
% ---------
% [connectCellExt] = CreateConnectivityCellExt(connectMatExt)

% created by   : Henning Schippke
% created on   : 20.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


elementNumbers        = connectMatExt(:,1);
connectivity          = connectMatExt(:,2:end);

nEle             = size(elementNumbers,1);
sizeArrayConnect = size(connectivity,2);
sizeArrayNEle    = size(elementNumbers,2);

extConnectivity  = [elementNumbers, connectivity,];

connectCellExt = mat2cell(extConnectivity, ones(1,nEle), [sizeArrayNEle, sizeArrayConnect]);
   
end %function

% ======================================================================= %
% ======================================================================= %