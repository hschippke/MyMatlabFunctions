function [] = ModifyLevelSetData(folder, nameCalcExample, levelSetFct)
% Function modifying the level set data (bc and ic).
% After calculating (x,y) value pairs of the zero level set via the
% provided levelSetFct, the shortest distance of each node from nodeCoordinates.csv
% is calculated to the zero level set. This data is stored in
% condBoundDir.csv and condInital.csv
%
%
% Input data:
% --------------
%
% folder: structure specifying input and output data folder
%     .inputDataFolder
%     .outputDataFolder
%
% nameCalcExmaple: string
%                  name of calculation example
%                  saved in condInitial.csv
%
% levelSetFct: function handle
%              Function calculating y values related to given x values
%              specifying the zero level set
%
%
% Output data:
% ---------------
% 
% [] : None
%
%
% Usage:
% ---------
% [] = ModifyLevelSetData(folder, nameCalcExample, levelSetFct)

% created  by Henning Schippke
% created  on 21.08.15
% modified on 22.08.15

% ======================================================================= %
% ======================================================================= %


inputPath  = folder.inputDataFolder;
outputPath = folder.outputDataFolder;

coord        = importdata([inputPath,'nodeCoordinates.csv'],',',7);
coord        = coord.data;

condBoundDir = importdata([inputPath,'condBoundDir.csv'],',',7);
condBoundDir = condBoundDir.data;

condInitial  = importdata([inputPath,'condInitial.csv'],',',7);
condInitial  = condInitial.data;


% create zero level set value pairs (x,y)
xMin    = min(coord(:,2));
xMax    = max(coord(:,2));

xLength = xMax - xMin;
xInterface = linspace(xMin,xMax,5000*xLength); % bessere Def. Aufloesung Interface ...

hInterface = levelSetFct(xInterface);


% calculate inital level set value for each node
[ initialPhi ] = LevelSet.CalcLevelSetFieldPoint2Point(coord, xInterface, hInterface);


% Set initial level set values
for nn = 1:size(initialPhi,1)
   
    nodeNumber    = initialPhi(nn,1);
    levelSetValue = initialPhi(nn,2);
    
    lineNo_NodeNumb          = find(condInitial(:,1) == nodeNumber);
    
    lineNo_dofTimeSlabBegin  = find(condInitial(:,2) == 6);    
    lineNo                   = intersect(lineNo_NodeNumb, lineNo_dofTimeSlabBegin);   
    condInitial( lineNo, 3 ) = levelSetValue;
    
    lineNo_dofTimeSlabBegin  = find(condInitial(:,2) == 26);
    lineNo                   = intersect(lineNo_NodeNumb, lineNo_dofTimeSlabBegin);  
    condInitial( lineNo, 3 ) = levelSetValue;
    
end

% save modifed initial condition data
FEM2FlowUtilities.CsvInputDataFiles.WriteNodeDataIc2Csv(outputPath, 'condInitial.csv', nameCalcExample, condInitial);


% Set boundary level set values
for nn = 1:size(condBoundDir,1)
   
    currLine = condBoundDir(nn,:);
    
    if currLine(2) == 6 || currLine(2) == 26
        
       nodeNumber = currLine(1);
       
       % get new LS value
       newLSValue = initialPhi( initialPhi(:,1) == nodeNumber, 2);
       
       condBoundDir(nn,3) = newLSValue;
        
    end
    
end

%WriteDBData(condBoundDir, csvDataPath);
FEM2FlowUtilities.CsvInputDataFiles.WriteNodeDataDirichlet2Csv(outputPath, 'condBoundDir.csv', nameCalcExample, condBoundDir);

end

% ================================================================= %
% ================================================================= %