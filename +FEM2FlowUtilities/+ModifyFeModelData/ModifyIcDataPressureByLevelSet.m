function [] = ModifyIcDataPressureByLevelSet(folder, nameCalcExample, modFct)
% Function modifying the pressure initial values depending on their level
% set value using the function modFct.
%
%
% Input data:
% --------------
%
% folder: structure specifying input and output data folder
%     .inputDataFolder
%     .outputDataFolder
%
% nameCalcExmaple: string
%                  name of calculation example
%                  saved in condInitial.csv
%
% modFct: function handle
%         function calculating the pressure initial conditions depending on
%         the level set value and its y-coordinate. The new pressure
%         initial value is returned.
%
%
% Output data:
% ---------------
% 
% [] : None
%
%
% Usage:
% ---------
% [] = ModifyIcDataPressureByLevelSet(folder, nameCalcExample, modFct)

% created  by Henning Schippke
% created  on 21.08.15
% modified on 22.08.15

% ======================================================================= %
% ======================================================================= %


inputPath  = folder.inputDataFolder;
outputPath = folder.outputDataFolder;

coord        = importdata([inputPath,'nodeCoordinates.csv'],',',7);
coord        = coord.data;

condInitial = importdata([inputPath,'condInitial.csv'],',',7);
condInitial = condInitial.data;

% look up table mapping pressure dof to level set dof
lutDofId  = [  4,  6; ...
              24, 26 ];

% loop over all initial conditions
for ii = 1:size(condInitial,1)
    
   nodeNumber = condInitial(ii,1);
   dofId      = condInitial(ii,2);  
   
   if dofId == 4 || dofId == 24
       
       % get corresponding level set value
       initialValNode = condInitial(    condInitial(:,1) == nodeNumber, 2:end );
       levelSetValue  = initialValNode( initialValNode(:,1) == lutDofId( lutDofId(:,1) == dofId, 2 ), 2 );
       
       if size(levelSetValue) > 1
           error('size(levelSetValue) > 1 : More than 1 level set value per node specified!')
       end
       
       
       yCoord   = coord( coord(:,1) == nodeNumber, 3);
       
       % calculate new pressure initial value
       pressureValue = modFct(levelSetValue, yCoord);
       
       condInitial(ii,3) = pressureValue;
       
   end
    
end

% writing new initial condition data to file
FEM2FlowUtilities.CsvInputDataFiles.WriteNodeDataIc2Csv(outputPath, 'condInitial.csv', nameCalcExample, condInitial);

end %function