function [] = ModifyBcData(folder, modValue, nameCalcExample, modFct)
% Function modifying bc data in condBounDir.csv.
%
%
% Input data:
% --------------
% 
% folder: structure specifying input and output data folder
%     .inputDataFolder
%     .outputDataFolder
%
% modValue: float
%           value specyfing which data rows in condBoundDir.csv will be
%           modified
%
% nameCalcExmaple: string
%                  name of calculation example
%                  saved in condBoundDir.csv
%
% modFct: function handle
%         function calculating the new value for teh specific bc row
%         Input arguments are the node coordinates.
%         Output arguments depend on which dof is recalculated.
%         velocity: 2 or 3 output arguments
%         pressure/temperature/levelSet: 1 output argument
%
%
% Output data:
% ---------------
% 
% [] : None
%
%
% Usage:
% ---------
% [] = ModifyBcData(folder, modValue, modFct)

% created  by Henning Schippke
% created  on 13.08.15
% modified on 22.08.15

% ======================================================================= %
% ======================================================================= %


inputPath  = folder.inputDataFolder;
outputPath = folder.outputDataFolder;

% load node coordinates
coord        = importdata([inputPath,'nodeCoordinates.csv'],',',7);
coord        = coord.data;

% load bc data
condBoundDir = importdata([inputPath,'condBoundDir.csv'],',',7);
condBoundDir = condBoundDir.data;


% loop over all boundary entries
for nn = 1:size(condBoundDir) 
    
    nodeNumber = condBoundDir(nn,1);
    dofId      = condBoundDir(nn,2);
    value      = condBoundDir(nn,3);
    
    if value == modValue % value must be corrected
       
        % get corresponding coordinate  
        xCoord = coord( coord(:,1) == nodeNumber, 2);
        yCoord = coord( coord(:,1) == nodeNumber, 3);
        
        % calculate new velocity values
        outValues          = cell(nargout(modFct),1);
        [outValues{1:end}] = modFct(xCoord, yCoord);
        
            
        if dofId == 1 || dofId == 21 % vx            
            condBoundDir(nn,3) = outValues{1};
        end
        
        if dofId == 2 || dofId == 22 % vy      
            condBoundDir(nn,3) = outValues{2};           
        end
        
        if dofId == 3 || dofId == 23 % vz     
            condBoundDir(nn,3) = outValues{3};           
        end
        
        if dofId == 4 || dofId == 24 % pressure      
            condBoundDir(nn,3) = outValues{1};           
        end
        
        if dofId == 5 || dofId == 25 % temperature          
            condBoundDir(nn,3) = outValues{1};           
        end
        
        if dofId == 6 || dofId == 26 % levelSet
            condBoundDir(nn,3) = outValues{1};           
        end
            
    end %if
    
    
end %for

FEM2FlowUtilities.CsvInputDataFiles.WriteNodeDataDirichlet2Csv(outputPath, 'condBoundDir.csv', nameCalcExample, condBoundDir);

end %function

% ======================================================================= %
% ======================================================================= % 