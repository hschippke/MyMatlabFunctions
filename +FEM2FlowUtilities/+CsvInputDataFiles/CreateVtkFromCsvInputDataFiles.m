function [] = CreateVtkFromCsvInputDataFiles(currPath, preProcFlag, varargin)
% Function creating vtk files with fe mesh and element data as well as
% boundary and initial (node) conditions from FEM2Flow csv input data files.
%
% Note: The folder 'MyMatlabFunctions' must be added to the Matlab path
% in order to use this function.
%
% Input data:
% --------------
%
% currPath: string containing the path of the file calling this function (no trailing /)
%
% preProcFlag: string
%              flag specifing which type of csv files will be transleted
%              'cubit': csv files directly after extraction from Cubit
%              'reordered': csv files after rev. Cuthill-McKee reordering
%
% % optional input arguments (key,value):
% * 'parallel': 0 or 1, specify if creation of vtk files
%               will be performed in parallel using Matlabs
%               Parallel Toolbox (parpool or matlabpool)
%               default: 1 (= yes)
%
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = CreateVtkInputDataFilesFromCsv(currPath, preProcFlag)

% created by:    Henning Schippke
% created on:    05.02.16
% last modified: 20.04.17

% TO DO:
% * ...

% ----------------------------------------------------------------- %
% ----------------------------------------------------------------- %


% -------------------------------------------------------------------
% Optional input arguments
% -------------------------------------------------------------------

% parallelExecution
defaultParallelExecution = 1;
checkParallelExecution   = @isnumeric;

p = inputParser;

p.addRequired('currPath');
p.addRequired('preProcFlag');

p.addParamValue('parallel', defaultParallelExecution, checkParallelExecution);

p.parse(currPath, preProcFlag, varargin{:});

parallelExecution = p.Results.parallel;

% -------------------------------------------------------------------
% end | Optional input arguments
% -------------------------------------------------------------------


disp('... generating vtk files for analysing input data');


if strcmp(preProcFlag,'cubit')
    inputFolder = [currPath,'/FeModelDataModified/'];

elseif strcmp(preProcFlag,'reordered')
    inputFolder = [currPath,'/FeModelInputData/'];

end

% -- Mesh data ---------------------------- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = 'MeshMatLoad';
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 1;

vtk.ParseCsv2vtkMeshData(inputFolder, vtkFileData, parallelExecution);

% ----------------------------------------- %


% -- Dirichlet boundary condition --------- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['CondBoundDir','_dofID-'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'condBoundDir.csv';

vtk.ParseCsv2vtkNodeData(inputFolder, vtkFileData, fileName, parallelExecution );
% ----------------------------------------- %


% -- Initial condition -------------------- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['CondInitial','_dofID-'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'condInitial.csv';

vtk.ParseCsv2vtkNodeData(inputFolder, vtkFileData, fileName, parallelExecution );
% ----------------------------------------- %


% -- Movable mesh data :: movable nodes --- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['MovMesh','_Nodes'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'meshMovNodes.csv';

if exist([inputFolder, fileName], 'file') == 2
    vtk.ParseCsv2vtkMovDataNode(inputFolder, vtkFileData, fileName, parallelExecution );
end
% ----------------------------------------- %


% -- Movable mesh data :: movable boundary nodes --- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['MovMesh','_NodesBound'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'meshMovNodesBound.csv';

if exist([inputFolder, fileName], 'file') == 2
    vtk.ParseCsv2vtkMovDataNode(inputFolder, vtkFileData, fileName, parallelExecution );
end
% ----------------------------------------- %


% -- Movable mesh data :: interior nodes SSL  %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['MovMesh','_SSLNodesInt'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'meshMovSSLNodesInt.csv';

if exist([inputFolder, fileName], 'file') == 2
    vtk.ParseCsv2vtkMovDataNode(inputFolder, vtkFileData, fileName, parallelExecution );
end
% ----------------------------------------- %


% -- Movable mesh data :: SSL elements  --- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['MovMesh','_SSL'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 1;

fileName = 'meshMovSSL.csv';

if exist([inputFolder, fileName], 'file') == 2
    vtk.ParseCsv2vtkMovDataMesh(inputFolder, vtkFileData, parallelExecution);
end
% ----------------------------------------- %


% -- Movable mesh data :: First node (locally) of SSL elements  --- %

vtkFileData.outputDataFolder    = [currPath,'/Visualisation/vtk/'];
vtkFileData.outputFileName      = ['MovMesh','_SSLNodeA'];
vtkFileData.flag.writePointData = 1;
vtkFileData.flag.writeCellData  = 0;

fileName = 'meshMovSSL.csv';

if exist([inputFolder, fileName], 'file') == 2
    vtk.ParseCsv2vtkMovDataNode(inputFolder, vtkFileData, fileName, parallelExecution );
end
% ----------------------------------------- %

disp('... vtk file generation complete')

end
