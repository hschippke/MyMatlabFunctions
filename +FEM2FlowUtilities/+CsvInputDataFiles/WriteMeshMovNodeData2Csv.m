function [] = WriteMeshMovNodeData2Csv(pathToFolder, fileName, nameTestExample, nodeIDs)
% Function writing node data of movable nodes to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% ic:              nDirichletNodes x 3 matrix
%                  matrix containing node number with corresponding dofID
%                  and value
%                  [ nodeNumb | dofID | value ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteMeshMovNodeData2Csv(pathToFolder, fileName, nameTestExample, ic)

% created by   : Henning Schippke
% created on   : 16.04.17
% last modified: 05.05.17

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');

fprintf(fid,'# %s -- movable mesh :: nodal data \n', nameTestExample );
fprintf(fid,'# data created or modified using Matlab: %s, %2.0f:%2.0f:%2.0f\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );
fprintf(fid,'# \n');
fprintf(fid,'# number of nodes: %-6d\n', size(nodeIDs,1) );
fprintf(fid,'# \n');
fprintf(fid,'# %6s\n\n','nodeID');

for ii = 1:size(nodeIDs,1)
    fprintf(fid,'%6d\n', nodeIDs(ii) );
end

fclose(fid);  


end %function

% ======================================================================= %
% ======================================================================= %