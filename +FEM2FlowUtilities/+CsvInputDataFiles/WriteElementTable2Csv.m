function [] = WriteElementTable2Csv(pathToFolder, fileName, nameTestExample, elementTable)
% Function writing element table to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% elementTable:    nEle x 4 matrix
%                  matrix containing element number and element IDs
%                  [ elementNumber | elementTypeID |  matID | loadID ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteElementTable2Csv(pathToFolder, fileName, nameTestExample, elementTable)

% created by   : Henning Schippke
% created on   : 21.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');

fprintf(fid,'# %s -- element table \n', nameTestExample );
fprintf(fid,'# data created from *.m file: %s, %2.0f:%2.0f:%2.0f\n\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );

fprintf(fid,'# number of elements: %-6d\n\n', size(elementTable,1) );

fprintf(fid,'# %13s, %13s, %9s, %10s \n\n','elementNumber','elementTypeID','matID','loadID');

for ii = 1:size(elementTable,1)
    
    elementNumber = elementTable(ii,1);
    eleTypeId     = elementTable(ii,2);
    matId         = elementTable(ii,3);
    loadId        = elementTable(ii,4);

    fprintf(fid,'%-8d, %-4d, %-2d, %-2d \n',[elementNumber, eleTypeId, matId, loadId]);

end

fclose(fid);


end %function

% ======================================================================= %
% ======================================================================= %