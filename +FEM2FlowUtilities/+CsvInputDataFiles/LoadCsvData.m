function [varargout] = LoadCsvData(folderInputDataFiles, fileName, varargin)
% Description:
% ---------------
% Function loading data array from csv file including header lines with
% text and column labels.
%
% Mandatory input data:
% ------------------------
% folderInputDataFiles : string with path to input data folder
%
% fileName : string with csv file name
%
% Optional input data (varargin): key-value pairs 
% -----------------------
% 'delimiter' : character used to separate data in input data file
%               {. , ; 1blank 4blanks/1tab }
% 'linesIgnore' : integer specifying the number of lines to ignore, i.e.
%                 the number of lines which preced the data array
%
% Output data:
% ---------------
% varargout{1} = data        : matrix containing the data array
% varargout{2} = colHeaders  : nColHeader x 1 cell with nColHeader column
%                              headers
% varargout{3} = textData    : nTextString x 1 cell with nTextString text
%                              strings from the text block of the csv data file
%
% Usage:
% ---------
% [varargout] = LoadCsvData(folderInputDataFiles, fileName, varargin)

% created by   : Henning Schippke
% created on   : 15.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Input Parser ------------------------------------------------------- %

% optional input argument: delimiter
defaultDelimiter   = ',';
validDelimiter     = {'.',',',';',' ','    '};
checkDelimiter     = @(x) any( validatestring(x, validDelimiter) );

% optional input argument: lines to ignore
defaultLinesIgnore = 6;
checkLinesIgnore   = @isnumeric;

% create input parser object
p = inputParser;

% add required input parameters
p.addRequired('folderInputDataFiles');
p.addRequired('fileName');

% add optional input arguments
% n.b.: optional input arguments are defined as parameter values in order
% to specify them as key-values paires in arbitrary order
p.addParamValue('delimiter'  , defaultDelimiter  , checkDelimiter  );
p.addParamValue('linesIgnore', defaultLinesIgnore, checkLinesIgnore);

% parse input values to input parser
p.parse(folderInputDataFiles, fileName, varargin{:});

% get values of optional input arguments
delimiter   = p.Results.delimiter;
linesIgnore = p.Results.linesIgnore;

% ----------------------------------------------------------------------- %


% -- Load data ---------------------------------------------------------- %
try
    importedData = importdata([folderInputDataFiles,fileName],delimiter,linesIgnore);
catch %#ok<CTCH>
    warning('Warn:warn','! --- No data from csv-file has been read. --- !\nEmpty arrays are assigned to data, column headers and text data variables');  
    importedData.data       = [];
    importedData.textdata   = [];
    importedData.colheaders = [];   
end

% ----------------------------------------------------------------------- %


% -- Data array --------------------------------------------------------- %

try
    data = importedData.data;
catch
    data = [];
end

% ----------------------------------------------------------------------- %


% -- Column headers ----------------------------------------------------- %

try
    colHeadersImported = importedData.colheaders;
catch
    colHeadersImported = [];
end

% Remark:
% All text strings in colHeadersImported start with a '#' follwed by
% one single blank ' '. Both symbols will be cut off the text block.

colHeaders         = cell(1,length(colHeadersImported));

for ii = 1:length(colHeadersImported)
   
    entryColHeader = colHeadersImported{ii};
    
    charIgnore = find( entryColHeader == '#' | entryColHeader == ' ');
    
    charTake   = ones(1,length(entryColHeader));
    charTake(charIgnore) = 0; %#ok<FNDSB>
    
    colHeaders{ii} = entryColHeader(charTake == 1);
  
end

% ----------------------------------------------------------------------- %


% -- Text data ---------------------------------------------------------- %

try
    textDataImported = importedData.textdata;
catch
    textDataImported = [];
end

% Remark:
% textDataImported contains the text block of the imported csv data
% file. In this block the last line of textDataImported does contain the
% column headers which is separated from the real text block by one
% preceding blank line. Hence, in order to read out the information of the
% real text block only, only the lines till the 2nd last line of
% textDataImported have to be read out.
% Furthermore, text strings in textDataImported are only contained in the
% first column of the cell array.
% Finally, all text strings in textDataImported start with a '#' follwed by
% one single blank ' '. Both symbols will be cut off the text block.

cc = 1;
textData = cell(cc);

for ii = 1:size(textDataImported,1)-2   
    
    textLine = textDataImported{ii,1};
    
    if ~isempty(textLine)
        textData{cc} = textLine(3:end);
        cc = cc + 1;
    end
    
end

% ----------------------------------------------------------------------- %


% -- Output arguments --------------------------------------------------- %

varargout{1} = data;
varargout{2} = colHeaders;
varargout{3} = textData;

% ----------------------------------------------------------------------- %


end %function

% ======================================================================= %

% ======================================================================= %
% ======================================================================= %