function [] = WriteConnectivity2Csv(pathToFolder, fileName, nameTestExample, connect)
% Function writing node connectivity to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% connect:         nEle x 5 matrix
%                  matrix containing node number and coordinates
%                  [ elementNumber | nodeA | nodeB | nodeC  | nodeD ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteConnectivity2Csv(pathToFolder, fileName, nameTestExample, connect)

% created by   : Henning Schippke
% created on   : 22.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %

currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');

fprintf(fid,'# %s -- node connectivity \n', nameTestExample );
fprintf(fid,'# data created from *.m file: %s, %2.0f:%2.0f:%2.0f\n\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );

nEle  = size(connect,1);
nNode = length( unique( connect(:,2:end) ) );
fprintf(fid,'# number of elements: %-6d, number of nodes: %-6d\n\n', nEle, nNode );

fprintf(fid,'# %13s, %6s, %6s, %6s, %6s \n\n','elementNumber','nodeA','nodeB','nodeC','nodeD');

for ii = 1:size(connect,1)    
   fprintf(fid,'%-4d, %-8d, %-8d, %-8d, %-8d \n', connect(ii,:));
end

fclose(fid);


end %function

% ======================================================================= %
% ======================================================================= %