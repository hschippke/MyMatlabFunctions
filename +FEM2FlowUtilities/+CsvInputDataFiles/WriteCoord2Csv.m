function [] = WriteCoord2Csv(pathToFolder, fileName, nameTestExample, coord )
% Function writing node coordinates to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% coord:           nNodes x 4 matrix
%                  matrix containing node number and coordinates
%                  [ nodeNumber | xCoord | yCoord | zCoord ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteCoord2Csv( pathToFolder, fileName, nameTestExample, coord )

% created by   : Henning Schippke
% created on   : 21.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');
 
fprintf(fid,'# %s -- node coordinates \n', nameTestExample );
fprintf(fid,'# data created from *.m file: %s, %2.0f:%2.0f:%2.0f\n\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );

fprintf(fid,'# number of nodes: %-6d\n\n', size(coord,1) );

fprintf(fid,'# %10s, %6s, %6s, %6s \n\n','nodeNumber','xCoord','yCoord','zCoord');

for ii = 1:size(coord,1)    
   fprintf(fid,'%-4d, %-8.4f, %-8.4f, %-8.4f \n', coord(ii,:));
end

fclose(fid);


end %function

% ======================================================================= %
% ======================================================================= %