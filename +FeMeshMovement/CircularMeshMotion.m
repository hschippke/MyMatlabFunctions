function [nodes_t1] = CircularMeshMotion(rotCenter, angVel, nodes_t0, nodes_t1, nodesMov, timeCurr)
%% Function calculation the (new) node positions
%  at end of current time slab due to mesh movement.
%
% Input Arguments
% ---------------
%
% rotCenter: 1x2 float vector with coordinates center of rotations
%
% angVel: float, angular velocity in [1/s]
% 
% nodes_to: nNodes x 2 float matrix with node coordinates
%           at beginning of time slab
%           [ xCoord | yCoord ]
%
% nodes_t1: nNodes x 2 float matrix with node coordinates
%           at end of time slab
%           [ xCoord | yCoord ]
%
% nodesMov: nNodesMov x 1 float vector with node IDs of movable nodes
%           which positions have to be calculated
%
% timeCurr: 1 x 3 float vector with time positions in [s]
%           [ tBeginning, tMiddle, tEnd ]
%
%
% Return Values
% -------------
%
% % nodes_t1: nNodes x 2 float matrix with recalculated node coordinates
%           at end of time slab
%           [ xCoord | yCoord ]
%
%
% Usage
% -----
%
% [nodes_t1] = CalcNewNodePositions(rotCenter, angVel, nodes_t0, nodes_t1, nodesMov, timeCurr)

% created by         Henning Schippke
% created on         18.04.2017 (based on script created in 07/2014)
% last modifified on 19.04.2017

% ======================================================================= %
% ======================================================================= %

    x_M   = rotCenter(1);
    y_M   = rotCenter(2);
    
    omega = angVel;
    
    % loop over all movable nodes
    for kk = 1:size(nodesMov,1)
 
        r   = sqrt( (nodes_t0(nodesMov(kk),1) - x_M)^2 + (nodes_t0(nodesMov(kk),2) - y_M)^2 );
         
        % check if node is in 1st or 4th qudrant
        if nodes_t0(nodesMov(kk),1) >= x_M
            
            if nodes_t0(nodesMov(kk),2) >= y_M
                % node is in 1st quadrant
                phi = acos( abs((nodes_t0(nodesMov(kk),1) - x_M)) / r );
            
            else % nodes is in 4th quadrant  
                phi = acos( abs((nodes_t0(nodesMov(kk),1) - x_M)) / r );
                phi = 2*pi - phi;
            end         
        end %if
        
        % check if node is in 2nd or 3rd qudrant
        if nodes_t0(nodesMov(kk),1) < x_M
            
            if nodes_t0(nodesMov(kk),2) >= y_M
                % node is in 2nd qudrant
                phi = acos( abs((nodes_t0(nodesMov(kk),1) - x_M)) / r );
                phi = pi - phi;
                
            else % node is in 3rd qudrant
                phi = acos( abs((nodes_t0(nodesMov(kk),1) - x_M)) / r );
                phi = phi + pi;
            end
        end
    
        % set (new) node positions at end of current time slab
        nodes_t1(nodesMov(kk),1) = x_M + r * cos ( omega * (timeCurr(1,3)-timeCurr(1,1)) + phi );
        nodes_t1(nodesMov(kk),2) = y_M + r * sin ( omega * (timeCurr(1,3)-timeCurr(1,1)) + phi );

    end %for

end