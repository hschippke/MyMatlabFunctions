function [] = CreateFolder(pathToFolder, folderName)
% Function creating folder on given path.
%
% Input Data:
% pathToFolder: string specifying path to folder : '../path/to/folder/'
% folderName  : name of folder to be created     : 'folderName/'
%
% [] = CreateFolder(pathToFolder, folderName)

% created by Henning Schippke, 20.03.15
% last modification            20.03.15

    if isempty(dir([pathToFolder,folderName]))
        mkdir([pathToFolder,folderName]);
    end
    
    
end %function CreateFolder