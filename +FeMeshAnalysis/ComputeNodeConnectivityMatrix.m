function [ nodeConnectStruct, bandwidth ] = ComputeNodeConnectivityMatrix( connect )
% Function computing the node to node connectivity of a given (finite
% element) mesh yielding a connectivity matrix K.
%
% Input Data:
% connect:       m x 1 cell or m x nNodeEle matrix with connectivity data for
%                m elements with nNodesEle nodes per element. In case
%                <connect> is of cell type, it will be converted into a
%                matrix.
%
% Output Data:
% nodeConnectStruct: structure summarising the connectivity matrix data
%     .K           : connectivity matrix
%     .K_nz        : number of nonzeros entries in K
%     .K_dim       : dimension of K
%     .K_density   : ratio of nonzero entries w.r.t. zeros in K
% bandwith         : structure summarising bandwith info w.r.t. to K
%     .value       : max bandwith value
%     .node        : node number with max bandwith value
%     .relative    : relative bandwith w.r.t. to K_dim
%
% [ nodeConnectStruct, bandwidth ] = ComputeNodeConnectivityMatrix( coord, connect )

% created by Henning Schippke, 20.03.15
% last modification            20.03.15


if iscell(connect)
    connect = ConvertConnectivity_cell2mat(connect);
end

nodeNumb    = unique(connect);
maxNodeNumb = max(nodeNumb);
nNode       = length(nodeNumb);

% evaluate matrix density and bandwidth
I  = zeros(maxNodeNumb^2,1);
J  = zeros(maxNodeNumb^2,1);

cc = 1;
for ii = 1:nNode
    
    currNodeNumb = nodeNumb(ii);
    
    [eleNumb, ~]        = find(connect == currNodeNumb); % elements containing current node    
    adjacentNodes       = connect(eleNumb,:);  % neighbouring nodes of current node
    
    numbAdjacentNodes   = size(adjacentNodes,1)*size(adjacentNodes,2);
   
    I(cc:cc+numbAdjacentNodes-1) = currNodeNumb * ones(numbAdjacentNodes,1); % later used as row indices
    J(cc:cc+numbAdjacentNodes-1) = adjacentNodes(:);               % later used as col indices
    
    cc = cc + numbAdjacentNodes;
    
end

I = nonzeros(I);
J = nonzeros(J);
X = ones(length(I),1); % entry in connectivity matrix

K     = sparse(I,J,X);     % connectivity matrix
                           % row number == node number
                           % col number:
                           % 0 : node has no connection to ode with current
                           %     col number
                           % 1 : node has connection to node with current
                           %     col number
                           % main diagonal value = number of elements which
                           %                       share node with row number
                           
K_nz  = sum(nonzeros(K));       % number of nonzeros entries
K_dim = size(K,1);              % dimension of connectivity matrix
K_density = K_nz/K_dim^2 * 100; % density of connectivity matrix

nodeConnectStruct.K         = K;
nodeConnectStruct.K_nz      = K_nz;
nodeConnectStruct.K_dim     = K_dim;
nodeConnectStruct.K_density = K_density;

bandwidth.value  = 0; % (maximum) bandwith of connectivity matrix
bandwidth.node   = 0; % node number with (max) bandwith

for ii = 1:size(K,1)
    
    currRow = K(ii,:);
    
    nonZerosEntriesRow = find(currRow);
    bandwidthCurrRow   = max(nonZerosEntriesRow) - min(nonZerosEntriesRow);
    
    if bandwidthCurrRow > bandwidth.value
        bandwidth.value = bandwidthCurrRow;
        bandwidth.node  = ii;
    end

end

bandwidth.relative = bandwidth.value / nNode * 100; % relative bandwith

end %function Compute ConnectivityMatrix

% ======================================================================= %
% ======================================================================= %